# Customer Relationship Manager

## Running app locally

The application is build with Spring Bood using Maven and UI is made with Angular.

First you need to clone the project using: 

'git clone https://gitlab.com/ilea.vlad/customer-relationship-manager' - for Spring Boot application

'git clone https://gitlab.com/ilea.vlad/customer-relationship-manager-angular' -for UI application

Open the project in your IDE with the pom.xml file.

### Database configuration
To run this app you will need first a database Sql schema created.


      ```
      spring.datasource.url = jdbc:mysql://localhost:3306/testdb?useSSL=false

      spring.datasource.username = root
      
      spring.datasource.password = admin
      ```


Replace datasource.url in 'application.properties' located in resource folder with your database and schema details, also replace username and password.

### Start Server

After the database setup is done you can start the server running 'CustomerRelationshipManagerApplication' file.

### Run UI app

After the server is on you need to open the UI project and run `ng serve` on your teminal and then navigate to `http://localhost:4200/`

## Using the app

For details about how to use the app: https://docs.google.com/presentation/d/1QLc4xWFurUpt5b-IRgi9IILidsRZ7vn5bfE9GWCOroQ/edit#slide=id.p