package com.example.customerRelationshipManager.converter;

import com.example.customerRelationshipManager.policy.Policy;
import com.example.customerRelationshipManager.dto.PolicyDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PolicyMapper {
    public PolicyDTO entityToDto(Policy policy) {
        PolicyDTO dto = new PolicyDTO();
        dto.setId(policy.getId());
        dto.setTypes(policy.getTypes());
        dto.setSubtype(policy.getSubtype());
        dto.setNumber(policy.getNumber());
        dto.setPeriod(policy.getPeriod());
        dto.setStartDate(policy.getStartDate());
        dto.setEndDate(policy.getEndDate());
        dto.setFee(policy.getFee());
        dto.setClient(policy.getClient());
        return dto;
    }

    public List<PolicyDTO> entityToDto(List<Policy> policyList) {
        return policyList.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public Policy dtoToEntity(PolicyDTO dto) {
        Policy policy = new Policy();
        policy.setId(dto.getId());
        policy.setTypes(dto.getTypes());
        policy.setSubtype(dto.getSubtype());
        policy.setNumber(dto.getNumber());
        policy.setPeriod(dto.getPeriod());
        policy.setStartDate(dto.getStartDate());
        policy.setEndDate(dto.getEndDate());
        policy.setFee(dto.getFee());
        policy.setClient(dto.getClient());
        return policy;
    }

    public List<Policy> dtoToEntity(List<PolicyDTO> dto){
        return dto.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
