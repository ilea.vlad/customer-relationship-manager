package com.example.customerRelationshipManager.converter;

import com.example.customerRelationshipManager.dto.ObservationDTO;
import com.example.customerRelationshipManager.observation.Observation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ObservationMapper {
    public ObservationDTO entityToDto(Observation observation) {
        ObservationDTO dto = new ObservationDTO();
        dto.setId(observation.getId());
        dto.setDate(observation.getDate());
        dto.setMessage(observation.getMessage());
        dto.setClient(observation.getClient());
        return dto;
    }

    public List<ObservationDTO> entityToDto(List<Observation> observations) {
        return observations.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public Observation dtoToEntity(ObservationDTO dto) {
        Observation observation = new Observation();
        observation.setId(dto.getId());
        observation.setDate(dto.getDate());
        observation.setMessage(dto.getMessage());
        observation.setClient(dto.getClient());
        return observation;
    }

    public List<Observation> dtoToEntity(List<ObservationDTO> dto) {
        return dto.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
