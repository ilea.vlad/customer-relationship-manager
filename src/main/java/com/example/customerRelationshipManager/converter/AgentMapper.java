package com.example.customerRelationshipManager.converter;

import com.example.customerRelationshipManager.agent.Agent;
import com.example.customerRelationshipManager.dto.AgentDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AgentMapper {

    public AgentDTO entityToDto(Agent agent) {

        AgentDTO dto = new AgentDTO();

        dto.setId(agent.getId());
        dto.setUsername(agent.getUsername());
        dto.setPassword(agent.getPassword());
        dto.setEmail(agent.getEmail());
        dto.setRole(agent.getRole());
        dto.setPermission(agent.getPermission());

        return dto;
    }

    public List<AgentDTO> entityToDto(List<Agent> agents) {
        return agents.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public Agent dtoToEntity(AgentDTO dto) {

        Agent agent = new Agent();

        agent.setId(dto.getId());
        agent.setUsername(dto.getUsername());
        agent.setPassword(dto.getPassword());
        agent.setEmail(dto.getEmail());
        agent.setRole(dto.getRole());
        agent.setPermission(dto.getPermission());

        return agent;
    }

    private List<Agent> dtoToEntity(List<AgentDTO> dto) {
        return dto.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
