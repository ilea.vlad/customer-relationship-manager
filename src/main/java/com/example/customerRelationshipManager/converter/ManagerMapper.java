package com.example.customerRelationshipManager.converter;

import com.example.customerRelationshipManager.dto.ManagerDTO;
import com.example.customerRelationshipManager.manager.Manager;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ManagerMapper {

    public ManagerDTO entityToDto(Manager manager) {

        ManagerDTO dto = new ManagerDTO();
        dto.setId(manager.getId());
        dto.setUsername(manager.getUsername());
        dto.setPassword(manager.getPassword());
        dto.setRole(manager.getRole());

        return dto;
    }

    public List<ManagerDTO> entityToDto(List<Manager> managers) {
        return managers.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public Manager dtoToEntity(ManagerDTO dto) {
        Manager manager = new Manager();
        manager.setId(dto.getId());
        manager.setUsername(dto.getUsername());
        manager.setPassword(dto.getPassword());
        manager.setRole(dto.getRole());

        return manager;
    }

    public List<Manager> dtoToEntity(List<ManagerDTO> dto) {
        return dto.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
