package com.example.customerRelationshipManager.converter;

import com.example.customerRelationshipManager.client.Client;
import com.example.customerRelationshipManager.dto.ClientDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClientMapper {

    public ClientDTO entityToDto(Client client) {
        ClientDTO dto = new ClientDTO();
        dto.setId(client.getId());
        dto.setName(client.getName());
        dto.setCnp(client.getCnp());
        dto.setPhoneNumber(client.getPhoneNumber());
        dto.setEmailAddress(client.getEmailAddress());
        dto.setGdprStatus(client.getGdprStatus());
        dto.setAddingDate(client.getAddingDate());
        dto.setLeadStatus(client.getLeadStatus());
        return dto;
    }

    public List<ClientDTO> entityToDto(List<Client> clients){
        return clients.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public Client dtoToEntity(ClientDTO dto) {
        Client client = new Client();
        client.setId(dto.getId());
        client.setName(dto.getName());
        client.setCnp(dto.getCnp());
        client.setPhoneNumber(dto.getPhoneNumber());
        client.setEmailAddress(dto.getEmailAddress());
        client.setGdprStatus(dto.getGdprStatus());
        client.setAddingDate(dto.getAddingDate());
        client.setLeadStatus(dto.getLeadStatus());

        return client;
    }

    public List<Client> dtoToEntity(List<ClientDTO> dto){
        return dto.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
