package com.example.customerRelationshipManager.converter;

import com.example.customerRelationshipManager.dto.ReminderDTO;
import com.example.customerRelationshipManager.reminder.Reminder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReminderMapper {


    public ReminderDTO entityToDto(Reminder reminder) {
        ReminderDTO dto = new ReminderDTO();
        dto.setId(reminder.getId());
        dto.setDate(reminder.getDate());
        dto.setMessage(reminder.getMessage());
        dto.setStatus(reminder.getStatus());
        dto.setClient(reminder.getClient());
        return dto;
    }

    public List<ReminderDTO> entityToDto(List<Reminder> reminders) {
        return reminders.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }


    public Reminder dtoToEntity(ReminderDTO dto) {
        Reminder reminder = new Reminder();
        reminder.setId(dto.getId());
        reminder.setDate(dto.getDate());
        reminder.setMessage(dto.getMessage());
        reminder.setStatus(dto.getStatus());
        reminder.setClient(dto.getClient());
        return reminder;
    }

    public List<Reminder> dtoToEntity(List<ReminderDTO> dto){
        return dto.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
