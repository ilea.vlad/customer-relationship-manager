package com.example.customerRelationshipManager.observation;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import com.example.customerRelationshipManager.abstractClass.GenericServiceImpl;
import com.example.customerRelationshipManager.client.ClientServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ObservationServiceImpl extends GenericServiceImpl<Observation, Long> implements ObservationService {

    private ObservationRepository observationRepository;
    private ClientServiceImpl clientService;

    @Autowired
    public ObservationServiceImpl(GenericRepo<Observation, Long> genericRepo, ObservationRepository observationRepository, ClientServiceImpl clientService) {
        super(genericRepo);
        this.observationRepository = observationRepository;
        this.clientService = clientService;
    }

    @Override
    public List<Observation> getByClientId(Long clientId) {
        return observationRepository.getAllByClient_IdOrderByDate(clientId);
    }

    @Override
    public Observation createWithClient(Observation observation, Long clientId) {
        observation.setClient(clientService.get(clientId).get());
        return observationRepository.save(observation);
    }
}
