package com.example.customerRelationshipManager.observation;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObservationRepository extends GenericRepo<Observation, Long> {
    List<Observation> getAllByClient_IdOrderByDate(Long clientId);
}
