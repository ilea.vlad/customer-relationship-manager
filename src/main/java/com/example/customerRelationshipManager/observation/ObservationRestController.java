package com.example.customerRelationshipManager.observation;

import com.example.customerRelationshipManager.abstractClass.GenericController;
import com.example.customerRelationshipManager.abstractClass.GenericService;
import com.example.customerRelationshipManager.converter.ObservationMapper;
import com.example.customerRelationshipManager.dto.ObservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/observation")
public class ObservationRestController extends GenericController<Observation, Long> {

    private final ObservationService observationService;
    private final ObservationMapper observationMapper;

    @Autowired
    public ObservationRestController(@Qualifier("observationServiceImpl") GenericService<Observation, Long> genericService, ObservationService observationService, ObservationMapper observationMapper) {
        super(genericService);
        this.observationService = observationService;
        this.observationMapper = observationMapper;
    }

    @GetMapping("/client/{id}")
    public ResponseEntity<List<ObservationDTO>> getByClientId(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(observationMapper.entityToDto(observationService.getByClientId(id)));
    }

    @PostMapping("/{id}")
    public ResponseEntity<ObservationDTO> createWithClient(@RequestBody ObservationDTO observationDTO,
                                                        @PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(observationMapper.entityToDto(observationService.createWithClient(observationMapper.dtoToEntity(observationDTO), id)));
    }
}
