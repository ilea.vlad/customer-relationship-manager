package com.example.customerRelationshipManager.observation;

import java.util.List;

public interface ObservationService {
    List<Observation> getByClientId(Long clientId);
    Observation createWithClient(Observation observation,Long clientId);
}
