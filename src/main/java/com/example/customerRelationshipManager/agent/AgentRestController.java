package com.example.customerRelationshipManager.agent;

import com.example.customerRelationshipManager.abstractClass.GenericController;
import com.example.customerRelationshipManager.abstractClass.GenericService;
import com.example.customerRelationshipManager.util.JwtUtil;
import com.example.customerRelationshipManager.converter.AgentMapper;
import com.example.customerRelationshipManager.dto.AgentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/agent")
public class AgentRestController extends GenericController<Agent, Long> {

    private final AgentService agentService;

    private final JwtUtil jwtUtil;

    private final AuthenticationManager authenticationManager;

    private final AgentMapper agentMapper;


    @Autowired
    public AgentRestController(@Qualifier("agentServiceImpl") GenericService<Agent, Long> genericService, AgentService agentService
            , AuthenticationManager authenticationManager, JwtUtil jwtUtil, AgentMapper agentMapper) {
        super(genericService);
        this.agentService = agentService;
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.agentMapper = agentMapper;

    }

    @PostMapping("/authenticate")
    public String generateToken(@RequestBody AgentDTO agentDTO) throws Exception {
        try {
            Agent dbAgent = agentService.findByUsername(agentDTO.getUsername());

            if (dbAgent.getPermission()) {
                Agent agent = agentMapper.dtoToEntity(agentDTO);
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(agent.getUsername(), agent.getPassword())
                );
                return jwtUtil.generateToken(agent.getUsername());
            } else {
                throw new Exception("Wait for manager authorization");
            }
        } catch (Exception ex) {
            throw new Exception("invalid username/password");
        }
    }

    @PostMapping(path = "/register", params = "manager")
    public void register(@RequestBody AgentDTO agentDTO,
                         @RequestParam(name = "manager") String manager) {
        agentService.createWithManager(agentMapper.dtoToEntity(agentDTO), manager);
    }

    @GetMapping(params = {"manager"})
    public ResponseEntity<List<AgentDTO>> getUnchecked(@RequestParam(name = "manager") String manager) {
        return ResponseEntity.ok(agentMapper.entityToDto(agentService.findAllByManagerNameAndPermission(manager)));
    }

    @GetMapping(params = {"username", "permission"})
    public ResponseEntity<AgentDTO> setPermission(@RequestParam(name = "username") String username,
                                               @RequestParam(name = "permission") Boolean permission) {
        return ResponseEntity.ok(agentMapper.entityToDto(agentService.setPermission(username, permission)));
    }
    @GetMapping(path = "/check",params = "username")
    public ResponseEntity<Boolean> checkUserExist(@RequestParam(name = "username") String username){
        return ResponseEntity.ok(agentService.existUser(username));
    }

    @GetMapping(path = "/getAll", params ={"manager"} )
    public ResponseEntity<List<AgentDTO>> getAllByManager(@RequestParam(name = "manager") String managerUsername){
        return ResponseEntity.ok(agentMapper.entityToDto(agentService.getAllByManager(managerUsername)));
    }



}

