package com.example.customerRelationshipManager.agent;

import com.example.customerRelationshipManager.client.Client;
import com.example.customerRelationshipManager.manager.Manager;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Agent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String email;

    @Column
    private String role;

    @Column
    private Boolean permission;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(columnDefinition = "manager_id")
    private Manager manager;

    @OneToMany(mappedBy = "agent",fetch = FetchType.EAGER)
    private List<Client> clientList;
}
