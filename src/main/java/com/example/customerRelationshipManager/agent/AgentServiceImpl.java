package com.example.customerRelationshipManager.agent;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import com.example.customerRelationshipManager.abstractClass.GenericServiceImpl;
import com.example.customerRelationshipManager.manager.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgentServiceImpl extends GenericServiceImpl<Agent, Long> implements AgentService {

    private final AgentRepository agentRepository;
    private final ManagerService managerService;


    @Autowired
    public AgentServiceImpl(GenericRepo<Agent, Long> genericRepo, AgentRepository agentRepository, ManagerService managerService) {
        super(genericRepo);
        this.agentRepository = agentRepository;
        this.managerService = managerService;
    }

    @Override
    public Agent findByUsername(String username) {
        return agentRepository.findByUsername(username);
    }

    @Override
    public List<Agent> findByRole(String role) {
        return agentRepository.findAllByRole(role);
    }

    @Override
    public Agent createWithManager(Agent agent, String managerUsername) {
        agent.setRole("ROLE_AGENT");
        agent.setPermission(false);
        agent.setManager(managerService.findByUsername(managerUsername));
        return agentRepository.save(agent);
    }

    @Override
    public List<Agent> findAllByManagerNameAndPermission(String username) {
        return agentRepository.findAllByManager_UsernameAndAndPermission(username, false);
    }

    @Override
    public Agent setPermission(String userName, Boolean permission) {
        Agent agent = agentRepository.findByUsername(userName);
        agent.setPermission(permission);
        return agentRepository.save(agent);
    }

    @Override
    public Boolean existUser(String userName) {
        return agentRepository.existsAgentByUsername(userName);
    }

    @Override
    public List<Agent> getAllByManager(String username) {
        return agentRepository.getAgentByManager_Username(username);
    }


}
