package com.example.customerRelationshipManager.agent;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgentRepository extends GenericRepo<Agent, Long> {

    Agent findByUsername(String username);

    List<Agent> findAllByRole(String role);

    List<Agent> findAllByManager_UsernameAndAndPermission(String username, Boolean permission);

    Boolean existsAgentByUsername(String username);

    List<Agent> getAgentByManager_Username(String username);

}
