package com.example.customerRelationshipManager.agent;

import java.util.List;

public interface AgentService {
    Agent findByUsername(String username);

    List<Agent> findByRole(String role);

    Agent createWithManager(Agent agent, String managerUsername);

    List<Agent> findAllByManagerNameAndPermission(String username);

    Agent setPermission(String userName,Boolean permission);

    Boolean existUser(String userName);

    List<Agent> getAllByManager(String username);
}
