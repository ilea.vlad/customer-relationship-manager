package com.example.customerRelationshipManager.policy;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PolicyRepository extends GenericRepo<Policy, Long> {
    List<Policy> findByNumberLikeAndClient_Agent_Username(String number,String username);

    List<Policy> getByClient_Id(Long id);

    List<Policy> getAllByEndDateBetweenAndClient_Agent_UsernameOrderByEndDateAsc(LocalDate today, LocalDate otherDay, String username);




}
