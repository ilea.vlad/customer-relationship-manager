package com.example.customerRelationshipManager.policy;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import com.example.customerRelationshipManager.abstractClass.GenericServiceImpl;
import com.example.customerRelationshipManager.client.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PolicyServiceImpl extends GenericServiceImpl<Policy, Long> implements PolicyService {

    private final PolicyRepository policyRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public PolicyServiceImpl(GenericRepo<Policy, Long> genericRepo, PolicyRepository policyRepository, ClientRepository clientRepository) {
        super(genericRepo);
        this.policyRepository = policyRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public List<Policy> find(String number ,String username) {
        return policyRepository.findByNumberLikeAndClient_Agent_Username("%" + number + "%",username);
    }

    @Override
    public List<Policy> getByClientId(Long id) {
        return policyRepository.getByClient_Id(id);
    }

    @Override
    public Policy createWithClient(Policy policy, Long clientId) {
        policy.setClient(clientRepository.getOne(clientId));
        return policyRepository.save(policy);
    }

    @Override
    public Policy updateWithClient(Policy policy, Long clientId) {
        policy.setClient(clientRepository.getOne(clientId));
        return policyRepository.save(policy);
    }

    @Override
    public List<Policy> getExpiredIn5Days(LocalDate today, String username) {
        return policyRepository.getAllByEndDateBetweenAndClient_Agent_UsernameOrderByEndDateAsc(today,today.plusDays(5),username);
    }

}
