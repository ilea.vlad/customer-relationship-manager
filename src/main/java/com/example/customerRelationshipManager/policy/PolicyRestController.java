package com.example.customerRelationshipManager.policy;

import com.example.customerRelationshipManager.abstractClass.GenericController;
import com.example.customerRelationshipManager.abstractClass.GenericService;
import com.example.customerRelationshipManager.converter.PolicyMapper;
import com.example.customerRelationshipManager.dto.PolicyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin()
@RequestMapping("/policy")
public class PolicyRestController extends GenericController<Policy, Long> {

    private final PolicyService policyService;
    private final PolicyMapper policyMapper;

    @Autowired
    public PolicyRestController(@Qualifier("policyServiceImpl") GenericService<Policy, Long> genericService,
                                PolicyService policyService,PolicyMapper policyMapper) {
        super(genericService);
        this.policyService = policyService;
        this.policyMapper = policyMapper;
    }

    @GetMapping("/")
    public ResponseEntity<List<PolicyDTO>> find(@RequestParam("number") String number,
                                                @RequestParam("username") String username) {
        return ResponseEntity.ok(policyMapper.entityToDto(policyService.find(username,number)));
    }

    @GetMapping("/client/{id}")
    public ResponseEntity<List<PolicyDTO>> getByClientId(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(policyMapper.entityToDto(policyService.getByClientId(id)));
    }


    @PostMapping("/create/{id}")
    public ResponseEntity<PolicyDTO> create(@RequestBody PolicyDTO policyDto,
                                         @PathVariable("id") Long id) {

        return ResponseEntity.ok(policyMapper.entityToDto(policyService.createWithClient(policyMapper.dtoToEntity(policyDto), id)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PolicyDTO> updateWithClient(@PathVariable(name = "id") Long id,
                                                   @RequestBody PolicyDTO policyDTO) {
        return ResponseEntity.ok(policyMapper.entityToDto(policyService.updateWithClient(policyMapper.dtoToEntity(policyDTO), id)));
    }
}
