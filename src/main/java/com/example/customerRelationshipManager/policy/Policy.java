package com.example.customerRelationshipManager.policy;

import com.example.customerRelationshipManager.client.Client;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Policy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private com.example.customerRelationshipManager.policy.enums.types types;
    @Column
    private com.example.customerRelationshipManager.policy.enums.subtype subtype;
    @Column
    private String number;
    @Column
    private Integer period;
    @Column
    private LocalDate startDate;
    @Column
    private LocalDate endDate;
    @Column
    private Double fee;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(columnDefinition ="client_id")
    private Client client;
}
