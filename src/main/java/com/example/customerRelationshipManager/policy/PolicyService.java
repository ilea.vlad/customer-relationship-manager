package com.example.customerRelationshipManager.policy;

import java.time.LocalDate;
import java.util.List;

public interface PolicyService {
    List<Policy> find(String username, String number);

    List<Policy> getByClientId(Long id);

    Policy createWithClient(Policy policy, Long clientId);

    Policy updateWithClient(Policy policy, Long clientId);

    List<Policy> getExpiredIn5Days(LocalDate today,String username);
}
