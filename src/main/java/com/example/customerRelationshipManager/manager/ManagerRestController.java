package com.example.customerRelationshipManager.manager;

import com.example.customerRelationshipManager.abstractClass.GenericController;
import com.example.customerRelationshipManager.abstractClass.GenericService;
import com.example.customerRelationshipManager.converter.ManagerMapper;
import com.example.customerRelationshipManager.dto.ManagerDTO;
import com.example.customerRelationshipManager.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/manager")
public class ManagerRestController extends GenericController<Manager,Long> {

    private final ManagerService managerService;

    private final ManagerMapper managerMapper;

    private final JwtUtil jwtUtil;

    @Autowired
    public ManagerRestController(@Qualifier("managerServiceImpl") GenericService<Manager,Long> genericService,
                                 ManagerService managerService,
                                 JwtUtil jwtUtil,
                                 AuthenticationManager authenticationManager,
                                 ManagerMapper managerMapper) {
        super(genericService);
        this.managerService = managerService;
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
        this.managerMapper = managerMapper;
    }

    private final AuthenticationManager authenticationManager;
    @PostMapping("/authenticate")
    public String generateToken(@RequestBody ManagerDTO managerDTO) throws Exception {
        try {
            Manager manager = managerMapper.dtoToEntity(managerDTO);
            authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(manager.getUsername(), manager.getPassword())
                );
                return jwtUtil.generateToken(manager.getUsername());

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("invalid username/password");
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<ManagerDTO>> getAllManagers(){
        return ResponseEntity.ok(managerMapper.entityToDto(managerService.getAllManagers()));
    }


}
