package com.example.customerRelationshipManager.manager;

import com.example.customerRelationshipManager.agent.Agent;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Manager {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String role;



    @OneToMany(mappedBy = "manager",fetch = FetchType.EAGER)
    private List<Agent> agentList;


}
