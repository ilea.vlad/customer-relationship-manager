package com.example.customerRelationshipManager.manager;

import java.util.List;

public interface ManagerService {
    Manager findByUsername(String username);
    List<Manager> getAllManagers();
}
