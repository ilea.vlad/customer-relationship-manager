package com.example.customerRelationshipManager.manager;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import com.example.customerRelationshipManager.abstractClass.GenericServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManagerServiceImpl extends GenericServiceImpl<Manager, Long> implements ManagerService {

    private ManagerRepository managerRepository;

    @Autowired
    public ManagerServiceImpl(GenericRepo<Manager, Long> genericRepo, ManagerRepository managerRepository) {
        super(genericRepo);
        this.managerRepository = managerRepository;
    }

    @Override
    public Manager findByUsername(String username) {
        return managerRepository.findByUsername(username);
    }

    @Override
    public List<Manager> getAllManagers() {
        return managerRepository.findAll();
    }
}
