package com.example.customerRelationshipManager.manager;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepository extends GenericRepo<Manager,Long> {
     Manager findByUsername(String username);
}
