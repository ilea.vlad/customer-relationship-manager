package com.example.customerRelationshipManager.reminder;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import com.example.customerRelationshipManager.abstractClass.GenericServiceImpl;
import com.example.customerRelationshipManager.client.ClientServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ReminderServiceImpl extends GenericServiceImpl<Reminder, Long> implements ReminderService {

    private ReminderRepository reminderRepository;
    private ClientServiceImpl clientService;


    @Autowired
    public ReminderServiceImpl(GenericRepo<Reminder,
                Long> genericRepo, ReminderRepository reminderRepository, ClientServiceImpl clientService) {
        super(genericRepo);
        this.reminderRepository = reminderRepository;
        this.clientService = clientService;

    }

    @Override
    public List<Reminder> getByClientId(Long clientId) {
        return reminderRepository.getRemindersByClient_Id(clientId);
    }

    @Override
    public Reminder createWithClient(Long clientId, Reminder reminder) {
        reminder.setClient(clientService.get(clientId).get());
        return reminderRepository.save(reminder);
    }

    @Override
    public Reminder updateWithClient(Long clientId, Reminder reminder) {
        reminder.setClient(clientService.get(clientId).get());
        return reminderRepository.save(reminder);
    }

    @Override
    public List<Reminder> getUpcomingReminders(String username) {
        LocalDate today = LocalDate.now();
        return reminderRepository.getRemindersByDateIsGreaterThanAndDateIsLessThanEqualAndStatusAndClient_Agent_Username(today, today.plusDays(2), true,username);
    }

    @Override
    public List<Reminder> getTodayReminder(String username) {
        LocalDate today = LocalDate.now();
        return reminderRepository.getRemindersByDateAndStatusAndClient_Agent_Username(today,true,username);
    }

    @Override
    public List<Reminder> getPassedUncheckReminders(String username) {
        LocalDate today = LocalDate.now();
        return reminderRepository.getRemindersByDateBeforeAndStatusAndClient_Agent_Username(today.minusDays(1), true,username);
    }

}
