package com.example.customerRelationshipManager.reminder;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReminderRepository extends GenericRepo<Reminder, Long> {
    List<Reminder> getRemindersByClient_Id(Long clientId);
    List<Reminder> getRemindersByDateIsGreaterThanAndDateIsLessThanEqualAndStatusAndClient_Agent_Username(LocalDate today, LocalDate theDayAfter, Boolean status,String username);
    List<Reminder> getRemindersByDateAndStatusAndClient_Agent_Username(LocalDate today,Boolean status,String username);
    List<Reminder> getRemindersByDateBeforeAndStatusAndClient_Agent_Username(LocalDate today, Boolean status,String username);
}
