package com.example.customerRelationshipManager.reminder;

import java.util.List;

public interface ReminderService {
    List<Reminder> getByClientId(Long clientId);

    Reminder createWithClient(Long clientId, Reminder reminder);

    Reminder updateWithClient(Long clientId, Reminder reminder);

    List<Reminder> getUpcomingReminders(String username);

    List<Reminder> getTodayReminder(String username);

    List<Reminder> getPassedUncheckReminders(String username);

}
