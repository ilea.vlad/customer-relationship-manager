package com.example.customerRelationshipManager.reminder;

import com.example.customerRelationshipManager.client.Client;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Reminder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDate date;

    @Column
    private String message;

    @Column(columnDefinition = "boolean default true")
    private Boolean status;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Client client;

}
