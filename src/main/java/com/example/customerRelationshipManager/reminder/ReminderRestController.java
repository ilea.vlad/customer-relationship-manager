package com.example.customerRelationshipManager.reminder;

import com.example.customerRelationshipManager.abstractClass.GenericController;
import com.example.customerRelationshipManager.abstractClass.GenericService;
import com.example.customerRelationshipManager.converter.ReminderMapper;
import com.example.customerRelationshipManager.dto.ReminderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/reminder")
public class ReminderRestController extends GenericController<Reminder, Long> {

    private final ReminderService reminderService;
    private final ReminderMapper reminderMapper;

    @Autowired
    public ReminderRestController(@Qualifier("reminderServiceImpl") GenericService<Reminder, Long> genericService, ReminderService reminderService, ReminderMapper reminderMapper) {
        super(genericService);
        this.reminderService = reminderService;
        this.reminderMapper = reminderMapper;
    }

    @GetMapping("/client/{id}")
    public ResponseEntity<List<ReminderDTO>> getByClientId(
            @PathVariable(name = "id") Long clientId) {
        return ResponseEntity.ok(reminderMapper.entityToDto(reminderService.getByClientId(clientId)));
    }

    @PostMapping("/client/{id}")
    public ResponseEntity<ReminderDTO> createWithClient(@PathVariable(name = "id") Long id,
                                                     @RequestBody ReminderDTO reminderDTO) {
        return ResponseEntity.ok(reminderMapper.entityToDto(reminderService.createWithClient(id, reminderMapper.dtoToEntity(reminderDTO))));
    }

    @PutMapping("/client/{id}")
    public ResponseEntity<ReminderDTO> updateWithClient(@PathVariable(name = "id") Long id,
                                                     @RequestBody ReminderDTO reminderDTO) {
        return ResponseEntity.ok(reminderMapper.entityToDto(reminderService.updateWithClient(id, reminderMapper.dtoToEntity(reminderDTO))));
    }

    @GetMapping("/getUpcoming")
    public ResponseEntity<List<ReminderDTO>> getUpcoming(@RequestHeader("username") String username) {
        return ResponseEntity.ok(reminderMapper.entityToDto(reminderService.getUpcomingReminders(username)));
    }

    @GetMapping("/getToday")
    public ResponseEntity<List<ReminderDTO>> getToday(@RequestHeader("username") String username) {
        return ResponseEntity.ok(reminderMapper.entityToDto(reminderService.getTodayReminder(username)));
    }

    @GetMapping("/getPassed")
    public ResponseEntity<List<ReminderDTO>> getPassed(@RequestHeader("username") String username) {
        return ResponseEntity.ok(reminderMapper.entityToDto(reminderService.getPassedUncheckReminders(username)));
    }
}
