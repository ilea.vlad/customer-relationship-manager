package com.example.customerRelationshipManager.util;

import com.example.customerRelationshipManager.agent.AgentServiceImpl;
import com.example.customerRelationshipManager.policy.PolicyService;
import com.example.customerRelationshipManager.reminder.Reminder;
import com.example.customerRelationshipManager.reminder.ReminderService;
import com.example.customerRelationshipManager.agent.Agent;
import com.example.customerRelationshipManager.policy.Policy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;


@Component
public class ScheduledTasks {

    private final ReminderService reminderService;
    private final AgentServiceImpl agentService;
    private final PolicyService policyService;

    @Autowired
    public ScheduledTasks(ReminderService reminderService, AgentServiceImpl agentService,PolicyService policyService) {
        this.reminderService = reminderService;
        this.agentService = agentService;
        this.policyService = policyService;
    }


    @Scheduled(cron = "0 0 6 * *  *")
    private void sentReminders() {
        List<Agent> agentList = agentService.findByRole("ROLE_USER");
        for (Agent agent : agentList) {
            List<Reminder> reminders = reminderService.getTodayReminder(agent.getUsername());
            List<Policy> policies = policyService.getExpiredIn5Days(LocalDate.now(),agent.getUsername());
            if (!reminders.isEmpty() || !policies.isEmpty()) {

                UtilMethods.sendEmail(reminders,policies);
            }
        }

    }


}
