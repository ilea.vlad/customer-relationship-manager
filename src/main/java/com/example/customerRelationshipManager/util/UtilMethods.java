package com.example.customerRelationshipManager.util;

import com.example.customerRelationshipManager.reminder.Reminder;
import com.example.customerRelationshipManager.policy.Policy;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

public class UtilMethods {

    public static void sendEmail(List<Reminder> reminderList, List<Policy> policyList) {
        final String username = "service.email.crm@gmail.com";
        final String password = "parolaEmailService";
        StringBuilder stringBuilder = new StringBuilder("");

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.socketFactory.port", "465");
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");


        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("service.email.crm@gmail.com"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse("ilea_vladutz@yahoo.com")
            );
            message.setSubject("Today's Events");
            if (!reminderList.isEmpty()) {
                stringBuilder.append("Reminders\n---------------------------\n");
                for (Reminder reminder : reminderList) {
                    stringBuilder.append(reminder.getClient().getName()).append(" : ").append(reminder.getClient().getPhoneNumber())
                            .append("\n").append("Message : ").append(reminder.getMessage()).append("\n\n");

                }
            }
            if (!policyList.isEmpty()) {
                stringBuilder.append("policy\n---------------------------\n");
                for (Policy policy : policyList) {
                    stringBuilder.append(policy.getClient().getName()).append(" : ").append(policy.getClient().getPhoneNumber()).append("\n")
                            .append(policy.getSubtype()).append(" : ").append(policy.getNumber()).append(" expire in: ").append(policy.getEndDate()).append("\n\n");
                }
            }
            message.setText(stringBuilder.toString());

            Transport.send(message);


        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
