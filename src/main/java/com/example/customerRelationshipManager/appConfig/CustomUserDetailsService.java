package com.example.customerRelationshipManager.appConfig;

import com.example.customerRelationshipManager.agent.Agent;
import com.example.customerRelationshipManager.agent.AgentRepository;
import com.example.customerRelationshipManager.manager.Manager;
import com.example.customerRelationshipManager.manager.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final AgentRepository agentRepository;
    private final ManagerRepository managerRepository;

    @Autowired
    public CustomUserDetailsService(AgentRepository agentRepository,
                                    ManagerRepository managerRepository) {
        this.agentRepository = agentRepository;
        this.managerRepository = managerRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Agent agent = agentRepository.findByUsername(username);
        if (agent == null){
            Manager manager = managerRepository.findByUsername(username);
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(manager.getRole()));
            return new User(manager.getUsername(),manager.getPassword(),authorities);
        }else {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(agent.getRole()));
            return new User(agent.getUsername(),agent.getPassword(),authorities);
        }

    }
}
