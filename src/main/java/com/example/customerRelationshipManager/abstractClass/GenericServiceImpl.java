package com.example.customerRelationshipManager.abstractClass;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public abstract class GenericServiceImpl<T, N> implements GenericService<T, N> {

    private final GenericRepo<T, N> genericRepo;

    public GenericServiceImpl(GenericRepo<T, N> genericRepo) {
        this.genericRepo = genericRepo;
    }

    @Override
    public T create(T entity) {
        return genericRepo.saveAndFlush(entity);
    }

    @Override
    public List<T> getAll() {
        return genericRepo.findAll();
    }

    @Override
    public Optional<T> get(N id) {
        return genericRepo.findById(id);
    }

    @Override
    public T update(T entity) {
        return genericRepo.saveAndFlush(entity);
    }


    @Override
    public void delete(N id) {
        genericRepo.deleteById(id);
    }
}
