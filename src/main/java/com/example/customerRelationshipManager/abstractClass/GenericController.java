package com.example.customerRelationshipManager.abstractClass;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public abstract class GenericController<T,N> {

    private final GenericService<T,N> genericService;

    public GenericController(GenericService<T, N> genericService) {
        this.genericService = genericService;
    }


    @PostMapping
    public ResponseEntity<T> create(@RequestBody T entity) {
        return ResponseEntity.ok(genericService.create(entity));
    }

    @GetMapping
    public ResponseEntity<List<T>> getAll() {
        List<T> entities = genericService.getAll();
        if (entities.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(entities);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<T> getById(@PathVariable(name = "id") N id) {
        Optional<T> entity = genericService.get(id);
        return entity.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
    }

    @PutMapping
    public ResponseEntity<T> update(@RequestBody T entity){
        return ResponseEntity.ok().body(genericService.update(entity));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteStudent(@PathVariable(name = "id") N id) {
        genericService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
