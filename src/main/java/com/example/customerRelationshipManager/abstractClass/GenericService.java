package com.example.customerRelationshipManager.abstractClass;

import java.util.List;
import java.util.Optional;

public interface GenericService<T,N> {
    T create(T entity);
    List<T> getAll();
    Optional<T> get(N id);
    T update(T entity);
    void delete(N id);
}
