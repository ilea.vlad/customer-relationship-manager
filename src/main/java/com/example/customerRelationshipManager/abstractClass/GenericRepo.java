package com.example.customerRelationshipManager.abstractClass;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepo<T,N> extends JpaRepository<T,N> {
}
