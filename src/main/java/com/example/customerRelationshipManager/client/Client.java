package com.example.customerRelationshipManager.client;

import com.example.customerRelationshipManager.observation.Observation;
import com.example.customerRelationshipManager.policy.Policy;
import com.example.customerRelationshipManager.reminder.Reminder;
import com.example.customerRelationshipManager.agent.Agent;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String cnp;

    @Column
    private String phoneNumber;

    @Column
    private String emailAddress;

    @Column
    private Boolean gdprStatus;

    @Column

    private LocalDate addingDate;

    @Column
    private Boolean leadStatus;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "agent_id")
    private Agent agent;

    @OneToMany(mappedBy = "client")
    List<Policy> policyList = new ArrayList<>();

    @OneToMany(mappedBy = "client")
    List<Observation> observationList = new ArrayList<>();

    @OneToMany(mappedBy = "client")
    List<Reminder> reminderList = new ArrayList<>();
    public void setAddingDate(LocalDate addingDate) {
        this.addingDate = LocalDate.now();
    }
}
