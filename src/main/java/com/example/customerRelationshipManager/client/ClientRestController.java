package com.example.customerRelationshipManager.client;

import com.example.customerRelationshipManager.abstractClass.GenericController;
import com.example.customerRelationshipManager.abstractClass.GenericService;
import com.example.customerRelationshipManager.converter.ClientMapper;
import com.example.customerRelationshipManager.dto.ClientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("/client")
public class ClientRestController extends GenericController<Client, Long> {

    private final ClientService clientService;
    private final ClientMapper clientMapper;

    @Autowired
    public ClientRestController(@Qualifier("clientServiceImpl") GenericService<Client, Long> genericService,
                                ClientService clientService, ClientMapper clientMapper) {
        super(genericService);
        this.clientService = clientService;
        this.clientMapper = clientMapper;
    }

    @GetMapping(params = {"username"})
    public ResponseEntity<List<ClientDTO>> getByAgentUsername(@RequestParam(value = "username") String username) {
        return ResponseEntity.ok(clientMapper.entityToDto(clientService.getByAgentUsername(username)));
    }

    @GetMapping(params = {"username", "name", "cnp", "number"})
    public ResponseEntity<List<ClientDTO>> find(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "cnp") String cnp,
            @RequestParam(name = "number") String number
    ) {
        return ResponseEntity.ok().body(clientMapper.entityToDto(clientService.getClientByAgentUsernameAndNameLikeAndCnpLike(username, name, cnp, number)));
    }

    @GetMapping("/getByGdpr")
    public ResponseEntity<List<ClientDTO>> getByGdpr(@RequestParam("gdpr") Boolean gdpr) {

        return ResponseEntity.ok().body(clientMapper.entityToDto(clientService.getByGdpr(gdpr)));
    }

    @PostMapping(params = {"username"})
    public ResponseEntity<ClientDTO> createWithAgent(@RequestParam(name = "username") String username,
                                                     @RequestBody ClientDTO clientDTO) {
        return ResponseEntity.ok(clientMapper.entityToDto(clientService.createWithAgent(username, clientMapper.dtoToEntity(clientDTO))));
    }

    @PutMapping(params = {"username"})
    public ResponseEntity<ClientDTO> updateWithAgent(@RequestParam(name = "username") String username,
                                                     @RequestBody ClientDTO clientDto) {
        return ResponseEntity.ok(clientMapper.entityToDto(clientService.createWithAgent(username, clientMapper.dtoToEntity(clientDto))));
    }

    @GetMapping(path = "/checkCnp", params = {"cnp"})
    public ResponseEntity<Boolean> checkCnp(@RequestParam(name = "cnp") String cnp) {
        return ResponseEntity.ok(clientService.checkCnp(cnp));
    }

    @GetMapping(path = "/agentAchievement", params = {"agentId", "sinceDate", "untilDate"})
    public ResponseEntity<Map<String, Integer>> getAgentAchievement(@RequestParam(name = "agentId") String id,
                                                                    @RequestParam(name = "sinceDate") String sinceDate,
                                                                    @RequestParam(name = "untilDate") String untilDate) {
        return ResponseEntity.ok(clientService.agentAchievementClients(id, sinceDate, untilDate));

    }

}
