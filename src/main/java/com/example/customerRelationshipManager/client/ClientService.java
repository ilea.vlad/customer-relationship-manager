package com.example.customerRelationshipManager.client;

import java.util.List;
import java.util.Map;


public interface ClientService {

    List<Client> getClientByAgentUsernameAndNameLikeAndCnpLike(String username, String name, String cnp, String number);

    Client getByName(String name);

    Client getByCnp(String cnp);

    List<Client> getByGdpr(Boolean gdpr);

    List<Client> getByAgentUsername(String username);

    Client createWithAgent(String username, Client client);

    Client updateWithClient(String username, Client client);

    Boolean checkCnp(String cnp);

    Map<String, Integer> agentAchievementClients(String agentId, String sinceDate, String untilDate);
}
