package com.example.customerRelationshipManager.client;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import com.example.customerRelationshipManager.abstractClass.GenericServiceImpl;
import com.example.customerRelationshipManager.agent.AgentServiceImpl;
import com.example.customerRelationshipManager.policy.Policy;
import com.example.customerRelationshipManager.policy.PolicyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class ClientServiceImpl extends GenericServiceImpl<Client, Long> implements ClientService {

    private final ClientRepository clientRepository;
    private final PolicyServiceImpl policyService;
    private final AgentServiceImpl agentService;


    @Autowired
    public ClientServiceImpl(GenericRepo<Client, Long> genericRepo, ClientRepository clientRepository, PolicyServiceImpl policyService, AgentServiceImpl agentService) {
        super(genericRepo);
        this.clientRepository = clientRepository;
        this.policyService = policyService;
        this.agentService = agentService;
    }

    @Override
    public Client create(Client client) {
        if (client.getGdprStatus() == null) {
            client.setGdprStatus(false);
        }
        return clientRepository.save(client);
    }


    @Override
    public List<Client> getClientByAgentUsernameAndNameLikeAndCnpLike(String username, String name, String cnp, String number) {
        if ((name.equals("") && cnp.equals("")) && number != null) {
            List<Policy> policies = policyService.find("%" + number + "%",username);
            List<Client> clientList = new ArrayList<>();
            for (Policy policy : policies) {
                clientList.add(policy.getClient());
            }

            return clientList;
        } else {
            return clientRepository.getClientsByAgent_UsernameAndNameIsLikeAndCnpIsLike(username, "%" + name + "%", "%" + cnp + "%");
        }

    }

    @Override
    public Client getByName(String name) {
        return Optional.ofNullable(clientRepository.getByName(name)).get().get();
    }

    @Override
    public Client getByCnp(String cnp) {
        return Optional.ofNullable(clientRepository.getByCnp(cnp)).get().get();
    }

    @Override
    public List<Client> getByGdpr(Boolean gdpr) {
        return clientRepository.getByGdprStatus(gdpr);
    }

    @Override
    public List<Client> getByAgentUsername(String username) {
        return clientRepository.findByAgent_Username(username);
    }

    @Override
    public Client createWithAgent(String username, Client client) {
        client.setAddingDate(LocalDate.now());
        client.setAgent(agentService.findByUsername(username));
        if (client.getGdprStatus() == null) {
            client.setGdprStatus(false);
        }

        return clientRepository.save(client);
    }

    @Override
    public Client updateWithClient(String username, Client client) {
        client.setAgent(agentService.findByUsername(username));
        return clientRepository.save(client);
    }

    @Override
    public Boolean checkCnp(String cnp) {
        return clientRepository.existsByCnp(cnp);
    }

    @Override
    public Map<String, Integer> agentAchievementClients(String agentId, String sinceDate, String untilDate) {
        Map<String, Integer> achievements = new HashMap<>();
        int i = 1;
        for (LocalDate date = LocalDate.parse(sinceDate); date.isBefore(LocalDate.parse(untilDate)); date = date.plusDays(7)) {
            achievements.put("week " + i, clientRepository.countAllByAgent_IdAndAddingDateAfterAndAddingDateBefore(Long.parseLong(agentId), date, date.plusDays(7)));
            i++;
        }
        return achievements;
    }
}
