package com.example.customerRelationshipManager.client;

import com.example.customerRelationshipManager.abstractClass.GenericRepo;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@Repository
public interface ClientRepository extends GenericRepo<Client, Long> {
    Optional<Client> getByName(String name);
    Optional<Client> getByCnp(String cnp);
    List<Client> getByGdprStatus(Boolean gdpr);
    List<Client>  getClientsByAgent_UsernameAndNameIsLikeAndCnpIsLike(String username,String name,String cnp);
    List<Client> findByAgent_Username(String username);
    Boolean existsByCnp(String cnp);
    Integer countAllByAgent_IdAndAddingDateAfterAndAddingDateBefore(Long agentId, LocalDate sinceDate, LocalDate untilDate);

}
