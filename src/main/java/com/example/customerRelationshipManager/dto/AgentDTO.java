package com.example.customerRelationshipManager.dto;

import lombok.Data;

@Data
public class AgentDTO {
    private Long id;
    private String username;
    private String password;
    private String email;
    private String role;
    private Boolean permission;
}
