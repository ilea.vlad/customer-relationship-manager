package com.example.customerRelationshipManager.dto;

import lombok.Data;

@Data
public class ManagerDTO {
    private Long id;
    private String username;
    private String password;
    private String role;

}
