package com.example.customerRelationshipManager.dto;

import com.example.customerRelationshipManager.client.Client;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ObservationDTO {

    private Long id;
    private LocalDate date;
    private String message;
    @JsonIgnoreProperties(value = {"policyList","agent","observationList", "reminderList"},allowSetters = true)
    private Client client;
}
