package com.example.customerRelationshipManager.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ClientDTO {
    private Long id;
    private String name;
    private String cnp;
    private String phoneNumber;
    private String emailAddress;
    private Boolean gdprStatus;
    private LocalDate addingDate;
    private Boolean leadStatus;
}
