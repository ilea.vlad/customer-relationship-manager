package com.example.customerRelationshipManager.dto;

import com.example.customerRelationshipManager.client.Client;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDate;



@Data
public class PolicyDTO {


    private Long id;
    private com.example.customerRelationshipManager.policy.enums.types types;
    private com.example.customerRelationshipManager.policy.enums.subtype subtype;
    private String number;
    private Integer period;
    private LocalDate startDate;
    private LocalDate endDate;
    private Double fee;
    @JsonIgnoreProperties(value = {"policyList","agent","observationList", "reminderList"},allowSetters = true)
    private Client client;
}
