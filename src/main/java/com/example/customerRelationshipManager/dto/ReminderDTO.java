package com.example.customerRelationshipManager.dto;

import com.example.customerRelationshipManager.client.Client;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ReminderDTO {
    private Long id;
    private LocalDate date;
    private String message;
    private Boolean status;
    @JsonIgnoreProperties(value = {"policyList","agent","observationList", "reminderList"},allowSetters = true)
    private Client client;
}
