package com.example.customerRelationshipManager.client;

import com.example.customerRelationshipManager.agent.AgentServiceImpl;
import com.example.customerRelationshipManager.policy.Policy;
import com.example.customerRelationshipManager.policy.PolicyServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;


public class ClientServiceImplTest {
    @Mock
    private ClientRepository clientRepository;
    @Mock
    private PolicyServiceImpl policyService;
    @Mock
    private AgentServiceImpl agentService;
    @InjectMocks
    private ClientServiceImpl clientServiceImpl;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getClientByAgentUsernameAndNameLikeAndCnpLike_validNumber_noPoliciesByNumber() {
        // GIVEN - input data generation and mock data setup
        // WHEN - method that is tested invocation step
        List<Client> clientsResponse = clientServiceImpl.getClientByAgentUsernameAndNameLikeAndCnpLike("", "", "", "");
        // THEN - validation steps
//        Assertions.assertEquals();
        Assertions.assertNotNull(clientsResponse);
        Assertions.assertTrue(clientsResponse.isEmpty());
//        Assertions.assertEquals(0, clientsResponse.size());
    }

    @Test
    public void getClientByAgentUsernameAndNameLikeAndCnpLike_validNumber_withPoliciesByNumber() {

        Client client = new Client();
        client.setName("Paul");
        Policy policy = new Policy();
        policy.setNumber("123");
        policy.setClient(client);
        Client client2 = new Client();
        client2.setName("Radu");
        Policy policy2 = new Policy();
        policy2.setNumber("321");
        policy2.setClient(client2);
        Mockito.when(policyService.find(anyString(),anyString())).thenReturn(Arrays.asList(policy, policy2));

        List<Client> clients = clientServiceImpl.getClientByAgentUsernameAndNameLikeAndCnpLike("", "", "", "");

        Assertions.assertNotNull(clients);
        Assertions.assertEquals(2, clients.size());
        Assertions.assertEquals("Paul", clients.get(0).getName());
        Assertions.assertEquals("Radu", clients.get(1).getName());
    }

    @Test
    public void getClientByAgentUsernameAndNameLikeAndCnpLike_validNameAndCNP_PoliciesByNumber() {

        Client client = new Client();
        client.setName("Paul");
        Client client2 = new Client();
        client2.setName("Radu");
        Mockito.when(clientRepository.getClientsByAgent_UsernameAndNameIsLikeAndCnpIsLike(anyString(), anyString(), anyString())).thenReturn(Arrays.asList(client, client2));

        List<Client> clients = clientServiceImpl.getClientByAgentUsernameAndNameLikeAndCnpLike("", "name", "1234", null);

        Assertions.assertNotNull(clients);
        Assertions.assertEquals(2, clients.size());
        Assertions.assertEquals("Paul", clients.get(0).getName());
    }

    @Test
    public void create_gdprNull() {

        Client client = new Client();
        client.setGdprStatus(null);

        Mockito.when(clientRepository.save(client)).thenReturn(client);

        Client client1 = clientServiceImpl.create(client);

        Assertions.assertNotNull(client);
        Assertions.assertFalse(client1.getGdprStatus());

    }
    @Test
    public void create_gdprNotNull() {

        Client client = new Client();
        client.setGdprStatus(true);
        Mockito.when(clientRepository.save(client)).thenReturn(client);

        Client client1 = clientServiceImpl.create(client);

        Assertions.assertNotNull(client);
        Assertions.assertTrue(client1.getGdprStatus());

    }

}
